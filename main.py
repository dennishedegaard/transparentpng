#!/usr/bin/env python
# -*- coding: utf-8 -*-
from io import BytesIO
import webapp2

import transparentpng

DEFAULT_MAX_DEVIATION = 5


class MainHandler(webapp2.RequestHandler):
    def get(self):
        with open('index.html') as f:
            self.response.write(f.read())

    def post(self):
        _file = self.request.POST.get('file', '')
        max_deviation = self.request.POST.get('max_deviation', DEFAULT_MAX_DEVIATION)

        try:
            max_deviation = int(max_deviation)
        except ValueError:
            max_deviation = DEFAULT_MAX_DEVIATION

        if _file is None:
            return 'Bad arguments :)'

        # get POST data.
        data = _file.value

        # convert to bytebuffer.
        _input = BytesIO(data)

        # make transparent PNG.
        output = transparentpng.make_image_transparent(_input, max_deviation=max_deviation)

        # return the result.
        self.response.content_type = 'image/png'
        self.response.write(output.getvalue())

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=True)
