#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
A simple module that helps in converting jpg-images to transparent png-images.
The functionality is based on the Python Image Library.
"""

__author__ = 'Dennis Hedegaard <dennis@dhedegaard.dk>'
__version__ = '0.1 - 2013-02-18'

from io import BytesIO

from PIL import Image


def _get_numeric_difference(color1, color2):
    """
    Gets the numeric difference in 2 colors. The higher the number returned,
    the "bigger" the difference.

    :param color1:tuple A tuple with 3 fields (red, green, blue).
    :param color2:tuple A tuple with 3 fields (red, green, blue).
    """
    diff = 0

    for c1, c2 in zip(color1, color2):
        diff += max(c1, c2) - min(c1, c2)

    return diff


def make_image_transparent(input_data, max_deviation=5, color=(255, 255, 255)):
    """
    Reads an image as input, converting a specific color to be transparent.

    """
    image = Image.open(input_data)
    image = image.convert('RGBA')
    data = image.getdata()

    transparent_color = (color[0], color[1], color[2], 0)

    newData = []
    for e in data:
        if _get_numeric_difference(e[0:3], color) <= max_deviation:
            newData.append(transparent_color)
        else:
            newData.append(e)

    image.putdata(newData)

    result = BytesIO()
    image.save(result, 'PNG')

    return result


def main():
    import sys

    if not len(sys.argv) == 3:
        sys.stderr.write('Bad arguments, call as %s <input_filename> <output_filename>' % sys.argv[0])
        sys.exit(1)

    make_image_transparent(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    main()