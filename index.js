$(function() {
    "use strict";

    var form = $('form');

    var max_deviation_div  = $('#max_deviation_div', form);
    var max_deviation = $('#max_deviation', max_deviation_div);
    var max_deviation_error = $('#max_deviation_error', max_deviation_div);

    var file_div = $('#file_div', form);
    var file = $('#file', file_div);
    var file_error = $('#file_error', file_div);

    $('button[type=submit]', form).click(function(e) {
        e.preventDefault();

        var submit = true;

        // validate file input.
        if (file.val() === '' || file.val() === undefined) {
            file_error.text('Please select a file to convert!');
            file_div.addClass('error');
            submit = false;
        } else {
            file_error.text('');
            file_div.removeClass('error');
        }

        // validate max_deviation input.
        var _max_deviation = max_deviation.val();
        if (_max_deviation === '') {
            _max_deviation = max_deviation.attr('placeholder');
        }
        _max_deviation = parseInt(_max_deviation, 10);
        if (isNaN(_max_deviation)) {
            max_deviation_error.text('Input should be a number!');
            max_deviation_div.addClass('error');
            submit = false;
        } else if (_max_deviation < 0) {
            max_deviation_error.text('Input should be 0 or higher!');
            max_deviation_div.addClass('error');
            submit = false;
        } else {
            max_deviation_error.text('');
            max_deviation_div.removeClass('error');
        }

        if (submit === true) {
            form.submit();
        }
    });

    // Enable tooltips.
    $('span.label_tooltip').tooltip();
});